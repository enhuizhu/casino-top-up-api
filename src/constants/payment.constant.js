export const PYAMENT_SUCCESS = 'payment is successful!';
export const PYAMENT_FAIL = 'payment failed!';
export const SUCCESS_STATUS = 'success';
export const FAIL_STATUS = 'FAIL';
export const TOO_LESS_MONEY = 'amount must be more then 30 pence';