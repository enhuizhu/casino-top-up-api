"use strict";

const dbConfig = {
    "default": "mssql",

    "mongodb": {
    	DATABASE: "media"
    },

	"mysql": {
		HOST: "172.28.128.3",
		USER: "root",
		PASSWORD: "roowpw",
		DATABASE: "eshop"
	},

	"mssql": {
		HOST: "localhost",
		USER: "sa",
		PASSWORD: "Ab£12345678",
		DATABASE: "LzGameDB",
		PORT: 1433
	}
}

export {dbConfig};
