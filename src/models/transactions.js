"use strict";

import model from "../core/model.js";

const moment = require('moment');

module.exports = class transactions extends model{
	constructor() {
		super();
	}

	createdb() {
		const sql = `CREATE TABLE PaymentTransactions (
			tranactionId int NOT NULL IDENTITY(1,1) PRIMARY KEY,
			userId int,
			amout int,
			numberOfCoin int,
			transactionTime datetime,
			status varchar(100),
			message varchar(255) 
		);`;

		this.db.query(sql).then(result => {
			console.log(result);
		});
	}

	createTranction(userId, amout, numberOfCoin) {
		const sql = `insert into PaymentTransactions (
			userId,
			amout,
			numberOfCoin,
			transactionTime
		) values (
			${userId},
			${amout},
			${numberOfCoin},
			'${moment().toISOString()}'
		); select @@IDENTITY AS 'identity';`;

		return this.db.query(sql);
	}

	updateTransactionStatus(id, status, msg = '') {
		const sql = `update PaymentTransactions 
			set status='${status}', message='${msg}'
			where tranactionId=${id} 
		`;

		return this.db.query(sql);
	}

	updateUserCoin(userId, amount) {
		const sql = `update TUserInfo set WalletMoney=WalletMoney + ${amount} where UserID=${userId}`;
		return this.db.query(sql);
	}

	getTransactionHistory(userId) {
		const sql = `select * from 
			PaymentTransactions 
			where userId=${userId}
			order by transactionTime desc
		`;
		return this.db.query(sql);
	}
}