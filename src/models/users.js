"use strict";

import model from '../core/model.js';
import tokenService from '../services/tokenService';
import {
	USER_DOES_NOT_EXIST,
	PASSWORD_WRONG,
	DB_ERROR
} from '../constants/login.constants';

const md5 = require('md5');

module.exports = class users extends model{
	constructor() {
		super();
	}


	login(username) {
		const resultObj = {
			success: true
		};
		
		return new Promise((resolve, reject) => {
			this.db.query(`select top 1 * from TUsers where UserID='${username}'`).then(result => {
				
				if (result.length < 1) {
					resultObj.success = false;
					resultObj.msg = `${username} ${USER_DOES_NOT_EXIST}`;
					resolve(resultObj);
					return ;
				}

				// const md5Pass = md5(password);

				// if (result[0].Pass !== md5Pass) {
				// 	resultObj.success = false;
				// 	resultObj.msg = PASSWORD_WRONG;
				// 	resolve(resultObj);
				// 	return ;
				// }
				
				resultObj.success = true;
				resultObj.token = tokenService.getToken(username);
				resolve(resultObj);
				return ;
			}).catch(e => {
				console.error(e);
				resultObj.success = false;
				resultObj.msg = DB_ERROR;
				resolve(resultObj);
				return ;
			});
		});
	}

	getUser(username, callback) {
		this.db.query(`select top 1 * from TUsers where UserID='${username}'`).then(result => {
			callback(result);
		});
	}

	getUserById(userId) {
		const sql = `select top 1 
			a.UserID, 
			UserName, 
			NickName,
			WalletMoney,
			LastLoginTM,
			OccuPation 
			from TUsers as a 
			left join TUserInfo as b on a.UserID = b.UserID
			where a.UserID = ${userId}
		`;
		return this.db.query(sql);
	}
}