"use strict";
import auth from "../middlewares/auth.js";
import { paths } from "../config/pathConfig.js";
import { rate } from '../config/exChangeRateConfig';

export default class route {
	constructor(app) {
		this.controllers = {};
		this.app = app;
		this.setRoute();
	}

	response(obj, req, res, params = []) {
		if( typeof this.controllers[ obj.controller ] == 'undefined' ) {
			var controller = require("../controllers/" + obj.controller);	
			this.controllers[ obj.controller ] = new controller();
		}

		this.controllers[obj.controller].setReqRes(req, res);
		this.controllers[obj.controller][obj.action](...params);
	}

	setRoute() {
		let multer  = require('multer')
		/**
		* need absolute path, otherwise when move to other plateform
		* the path will be different
		**/
		let upload = multer({ dest: paths.VIDEO_UPLOAD_PATH});
		
		this.app.get('/',  (req, res) => {
			this.response({
				controller: 'main',
				action: 'index'
			}, req, res);
		});

		this.app.get('/user/profile', auth, (req, res) => {
			this.response({
				controller: 'user',
				action: 'profile'
			}, req, res);
		});

		this.app.post('/login', (req, res) => {
			this.response({
				controller: 'login',
				action: 'index',
			}, req, res);
		});

		this.app.post('/charge', auth, (req, res) => {
			this.response({
				controller: 'charge',
				action: 'index',
			}, req, res);
		});

		this.app.get('/charge/history', auth, (req, res) => {
			this.response({
				controller: 'charge',
				action: 'history'
			}, req, res);
		});

		this.app.get('/exchangeRate', (req, res) => {
			res.send({
				rate
			});
		});

		this.app.get('/createdb', (req, res) => {
			this.response({
				controller: 'charge',
				action: 'createdb'
			}, req, res);
		});
	}
}