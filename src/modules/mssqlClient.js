"use strict";

import {dbConfig} from "../config/dbConfig";
// Retrieve
const sql = require('mssql');
// Connect to the db
let sqlPool = {db: null};

const config = {
    user: dbConfig.mssql.USER,
    password: dbConfig.mssql.PASSWORD,
    server: dbConfig.mssql.HOST, // You can use 'localhost\\instance' to connect to named instance
    database: dbConfig.mssql.DATABASE,
    port: dbConfig.mssql.PORT,
    options: {
       encrypt: false // Use this if you're on Windows Azure
    }
}

sql.connect(config).then(pool => {
    console.log('have pool');
    sqlPool.db = pool;
}).catch(e => {
    console.log('mssql error:', e);
})

sql.on('error', err => {
    console.log('mssql error:', err);
})

module.exports = sqlPool;