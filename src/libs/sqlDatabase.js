const sql = require('mssql');

export default class sqlDatabase {  
  constructor(pool) {
    this.db = pool;
	}

	query(queryStr) {    
    return new Promise((resolve, reject) => {      
      this.db.request().query(queryStr)
        .then(result => {
          if (result.recordset 
            && result.recordset[0] 
            && result.recordset[0].identity) {
            resolve(result.recordset[0]);
          } else {
            resolve(result.recordsets[0]);
          }
        })
        .catch(err => {
          console.log('sql err in query', err);
          reject(err);
        });
      // sql.connect(this.config).then(pool => {
      //   // Query 
      //   return pool.request()
      //   .query(queryStr)
      // }).then(result => {
      //   if (result.recordset 
      //     && result.recordset[0] 
      //     && result.recordset[0].identity) {
      //     resolve(result.recordset[0]);
      //   } else {
      //     resolve(result.recordsets[0]);
      //   }
      // })
      // .catch(err => {
      //   reject(err);
      // })
      
      // sql.on('error', err => {
      //   // ... error handler
      //   reject(err);
      // })
    });
	}
}

