"use strict";

import controller from "../core/controller";
import { apiConfig } from '../config/apiConfig';
import { rate } from '../config/exChangeRateConfig';
import transactions from '../models/transactions';
import {
	PYAMENT_SUCCESS,
	PYAMENT_FAIL,
	SUCCESS_STATUS,
	FAIL_STATUS,
	TOO_LESS_MONEY
} from '../constants/payment.constant';

module.exports = class charge extends controller {
	constructor(req, res) {
		super();
		this.transactions = new transactions();
	}

	index() {
		console.log('token', this.req.body);
		console.log('id', this.req.userId);
		
		const stripLib = require('stripe');
		const stripe = stripLib(apiConfig.secrectKey);
		// Token is created using Checkout or Elements!
		// Get the payment token ID submitted by the form:
		const token = this.req.body.token; // Using Express
		const moneyToPay = this.req.body.amount;
		const amoutOfCoins = moneyToPay * rate;
		
		if (moneyToPay < 0.3) {
			this.sendErrorMsg(TOO_LESS_MONEY);
			return ;
		}

		this.transactions.createTranction(this.req.userId, moneyToPay, amoutOfCoins).then(result => {
			// this.sendSuccessMsg(PYAMENT_SUCCESS, true);
			const des = `digital gold coins ${this.req.body.amount}`;
			
			const charge = stripe.charges.create({
				amount: moneyToPay * 100,
				currency: 'gbp',
				description: des,
				source: token,
				statement_descriptor: 'digital gold coins',
				capture: true,
				metadata: {
					order_id: result.identity,
					amount: this.req.body.amount,
					userid: this.req.userId,
					username: this.req.username
				},
			}, (err, outcome) => {
				console.log('err', err);
				if (err) {
					this.transactions.updateTransactionStatus(result.identity, FAIL_STATUS, PYAMENT_FAIL)
					.then(result => {
						console.log('update fail status');
					});

					this.sendErrorMsg(err && err.message);
				} else {
					this.transactions.updateTransactionStatus(result.identity, SUCCESS_STATUS, PYAMENT_SUCCESS)
					.then(result => {
						this.transactions.updateUserCoin(this.req.userId, amoutOfCoins).then(() => {
							console.log('update result');
						}).catch(e => {
							console.log('update error', e);
						});
					});	
					this.sendSuccessMsg(PYAMENT_SUCCESS, true);
				}
			});
		}).catch(e => {
			this.sendErrorMsg('error');
		});
	}

	createdb() {
		this.transactions.createdb();
		this.res.send('creating db ...');
	}

	history() {
		this.transactions.getTransactionHistory(this.req.userId)
			.then(result => {
				console.log('result', result);
				this.res.send(result);
			}).catch(e => {
				console.log('fetch transactions failed!');
				this.sendErrorMsg('fetch transactions failed!');
			});
	}
}