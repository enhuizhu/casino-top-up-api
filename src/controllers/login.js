"use strict";

import controller from '../core/controller';
import users from '../models/users'; 
import {
	USERNAME_OR_PASSWORD_EMPTY
} from '../constants/login.constants';

module.exports = class login extends controller{
	constructor(req, res) {
		super();
		this.users = new users();
	}

	index() {
		let	loginObj = this.req.body;

		if (!loginObj.username) {
			this.sendErrorMsg(USERNAME_OR_PASSWORD_EMPTY);
			return ;
		}
		
		this.users.login(loginObj.username).then(result => {
			this.res.send(result);
		});
	}
}