"use strict";

import controller from "../core/controller";
import users from '../models/users';

module.exports = class user extends controller{
	constructor(req, res) {
		super();
		this.users = new users();
	}

	profile() {
		this.users.getUserById(this.req.userId).then(result => {
			this.res.send(result);
		});
	}
}